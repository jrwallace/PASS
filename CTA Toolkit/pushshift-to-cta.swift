// Copyright (c) 2024 Jim Wallace
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

import Foundation
import ArgumentParser
import SwiftNLP

@main
struct singleFile: AsyncParsableCommand {
    
    @Option(name: .shortAndLong, help: "The directory to read files from.")
    var inputDirectoryPath: String
    
    @Option(name: .shortAndLong, help: "The directory to write output files to.")
    var outputDirectoryPath: String
            
    @Flag(name: .shortAndLong, help: "Whether to output extra debug text.")
    var verbose: Bool = false
    
    @Option(name: .shortAndLong, help: "The size of input and output memory buffers to allocate for each file, in MB.")
    var bufferSize: Int = 1
    
    @Option(name: .shortAndLong, help: "The maximum number of concurrent child threads to use at once.")
    var threads: Int = max(ProcessInfo.processInfo.activeProcessorCount - 2, 1)
        
    
    /*
     
        MAIN Method
     
     */
    mutating func run() async throws {
        
        if verbose {
            print("""
                  
                  Loading files from: \(inputDirectoryPath)
                  Output: \(outputDirectoryPath)
                  Max Threads: \(threads)
                  
                  """)
        }
        
        // Get a List of all files in input folder
        let files = try FileManager.default.contentsOfDirectory(atPath: inputDirectoryPath)
        
        // Make sure the output folder is ready
        if !FileManager.default.fileExists(atPath: outputDirectoryPath) {
            try FileManager.default.createDirectory(at: URL(fileURLWithPath: outputDirectoryPath), withIntermediateDirectories: true, attributes: nil)
        }
                        
                        
        // Spawn a task for each file
        await withThrowingTaskGroup(of: Void.self) { taskGroup in
            
            var numActiveThreads = 0
            
            for f in files.sorted(by: >) {  // Try to iterate over submissions first, since they're smaller
                
                // Local captures for safe concurrency
                let inputURL = URL(fileURLWithPath: inputDirectoryPath + "/" + f)
                let outputURL = URL(fileURLWithPath: outputDirectoryPath + "/" + f.replacingOccurrences(of: ".zst", with: ".json"))
                let verbose = verbose
                                                               
                // If we've maxed out our thread count, wait
                if numActiveThreads >= threads {
                    try? await taskGroup.next()
                    numActiveThreads -= 1
                }
                                
                if f.contains("comment") || f.contains("RC_") {

                    numActiveThreads += 1
                    
                    // Spawn a separate process for this file
                    taskGroup.addTask {
                        
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        
                        if verbose {
                            print("\(formatter.string(from: Date())) START: \(f)")
                        }
                        
                        let (posts, _): ([Comment],[Data]) = try await loadFromRedditArchive(inputURL)
                        let output = RedditCommentData(posts)
                        
                        do {
                            let jsonData = try JSONEncoder().encode(output)
                            
                            // HACK
                            let jsonString = String(data: jsonData, encoding: .utf8)
                            let fixedString = jsonString?.replacingOccurrences(of: "\"[END OF INDEX]\",", with: "\n")
                            
                            try fixedString?.write(to: outputURL, atomically: true, encoding: .utf8)
//                            try jsonData.write(to: outputURL)
                        }
                                                
                        if verbose {
                            print("\(formatter.string(from: Date())) STOP: \(f)")
                        }
                    }
                }
                
                if f.contains("submission") || f.contains("RS_") {
                    numActiveThreads += 1
                    
                    // Spawn a separate process for this file
                    taskGroup.addTask {
                        
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        
                        if verbose {
                            print("\(formatter.string(from: Date())) START: \(f)")
                        }
                        
                        let (posts, _): ([Submission],[Data]) = try await loadFromRedditArchive(inputURL)
                        let output = RedditSubmissionData(posts)
                        
                        do {
                            let jsonData = try JSONEncoder().encode(output)

                            // HACK
                            let jsonString = String(data: jsonData, encoding: .utf8)
                            let fixedString = jsonString?.replacingOccurrences(of: "\"[END OF INDEX]\",", with: "\n")
                            
                            try fixedString?.write(to: outputURL, atomically: true, encoding: .utf8)
//                            try jsonData.write(to: outputURL)
                        }
                        
                        if verbose {
                            print("\(formatter.string(from: Date())) STOP: \(f)")
                        }
                    }

                }
            }
        }
        
        if verbose {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            print("\(formatter.string(from: Date())) All files complete")
        }
        
    }
}



















