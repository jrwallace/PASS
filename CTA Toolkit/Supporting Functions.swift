import Foundation
import SwiftNLP

func loadFromRedditArchive<C: Decodable>( _ fileURL: URL, verbose: Bool = false) async throws -> (posts: [C], errors: [Data]) {
    
    var posts =  [C]()
    var errorData = [Data]()
    
                         
    let decoder = JSONDecoder()
                        
    let fileData = try! Data(contentsOf: fileURL)
    let splitData = splitDataIntoLines(data: fileData)

    // Error logging variables
    var lastData: Data? = nil
        
    for data in splitData {
        do {
            // Reset our error tracking variables
            lastData = data
            posts.append( try decoder.decode(C.self, from: data) )
            
        } catch {
            //numberOfErrors += 1
            errorData.append(lastData!)
        }
    }
    
    return (posts, errorData)
}


@inlinable
func splitDataIntoLines(data: Data) -> [Data] {
    var lines = [Data]()
    var lineStart = data.startIndex
    var lineEnd: Data.Index?
    var current = data.startIndex
    while current < data.endIndex {
        if data[current] == 10 { // ASCII newline
            lineEnd = current
            var line = data[lineStart..<lineEnd!]
            if line.last == 44 { line.removeLast() } // Remove trailing commas
            lines.append(line)
            lineStart = data.index(after: current)
        }
        current = data.index(after: current)
    }
    if lineStart < current {
        var line = data[lineStart..<current]
        
        if line.last == 44 { line.removeLast() } // Remove trailing commas
        
        lines.append(line)
    }
    return lines
}

public func splitByMonthAndYear<T: RedditDataItem>(list: [T]) -> [String: [T]] {
    
    var itemsByMonthYear: [String: [T]] = [:]

    for item in list {
        guard let createdUTC = item.created_utc else {
            print(item)
            continue // Skip submissions without a creation date
        }
        
        // Convert created_utc to a Date object
        let date = Date(timeIntervalSince1970: TimeInterval(createdUTC))
        
        // Define a date formatter to extract month and year
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM" // Format to group by month/year
        
        // Get the month/year string
        let monthYearString = dateFormatter.string(from: date)
        
        // Add submission to the corresponding month/year array
        if var itemsForMonthYear = itemsByMonthYear[monthYearString] {
            itemsForMonthYear.append(item)
            itemsByMonthYear[monthYearString] = itemsForMonthYear
        } else {
            itemsByMonthYear[monthYearString] = [item]
        }
    }

    return itemsByMonthYear
}
