//
//  Split by Month and Year.swift
//  split
//
//  Created by Jim Wallace on 2024-04-22.
//

import Foundation
import SwiftNLP

public func splitByMonthAndYear<T: RedditDataItem>(list: [T]) -> [String: [T]] {
    var itemsByMonthYear: [String: [T]] = [:]
    
    // Define date formatter outside the loop to reuse it
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM"
    
    for item in list {
        guard let createdUTC = item.created_utc else {
            continue // Skip submissions without a creation date
        }
        
        let date = Date(timeIntervalSince1970: TimeInterval(createdUTC))
        
        let monthYearString = dateFormatter.string(from: date)
        
        if itemsByMonthYear[monthYearString] == nil {
            itemsByMonthYear[monthYearString] = [item]
        } else {
            itemsByMonthYear[monthYearString, default: []].append(item)
        }
    }
    
    return itemsByMonthYear
}


func loadFromRedditArchive<C: Decodable>( _ fileURL: URL, verbose: Bool = false) async throws -> (posts: [C], errors: [Data]) {
    
    var posts =  [C]()
    var errorData = [Data]()
    
                         
    let decoder = JSONDecoder()
                        
    let fileData = try! Data(contentsOf: fileURL)
    let splitData = splitDataIntoLines(data: fileData)

    // Error logging variables
    var lastData: Data? = nil
        
    for data in splitData {
        do {
            // Reset our error tracking variables
            lastData = data
            posts.append( try decoder.decode(C.self, from: data) )
            
        } catch {
            //numberOfErrors += 1
            errorData.append(lastData!)
        }
    }
    
    return (posts, errorData)
}


@inlinable
func splitDataIntoLines(data: Data) -> [Data] {
    var lines = [Data]()
    var lineStart = data.startIndex
    var lineEnd: Data.Index?
    var current = data.startIndex
    while current < data.endIndex {
        if data[current] == 10 { // ASCII newline
            lineEnd = current
            var line = data[lineStart..<lineEnd!]
            if line.last == 44 { line.removeLast() } // Remove trailing commas
            lines.append(line)
            lineStart = data.index(after: current)
        }
        current = data.index(after: current)
    }
    if lineStart < current {
        var line = data[lineStart..<current]
        
        if line.last == 44 { line.removeLast() } // Remove trailing commas
        
        lines.append(line)
    }
    return lines
}
